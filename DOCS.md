# Leader_lib Library

A library for managing leaderboards.

## Usage

### Creating a Leaderboard

```lua
leader_lib.create_leaderboard(record_template, options)
```

- `record_template` (table): A template for the leaderboard record with default values.
  Example: `{p_name = "singleplayer", score = 0}`. A `timestamp` attribute will be automatically created and
  updated when the record is updated.
  
- `options` (optional) (table):
  - `uid_key` (string): An attribute that should not be repeated on the leaderboard. Usually, you should set it to a key that stores the player's name, assuming the leaderboard stores unique player scores. The default is `"timestamp"`. 
  - `ignore_threshold` (number): Default: `0`. The `add_record` function is ignored if the value of the `sort_key` attribute is at or below (or above, if `sort_key` is set to "descending") this value. Useful for ignoring scores that are too low.
  - `max_entries` (integer): Default: `-1`. `-1` means no limit. Otherwise, the number of slots is limited to this amount. Using `-1` risks allowing the leaderboard to grow too large to handle efficiently or even too large to keep in memory (though this would only happen on a large server).
  - `sort_key` (string): Default: `"timestamp"`. The attribute key by which records are removed if there is a maximum record limit. 
  - `sort_direction` (string): Default: `"ascending"`. Either `"ascending"` (smaller values are removed first) or `"descending"` (larger values are removed first) if there is a maximum record limit. This also sets the default sort direction if `leader_lib.get_sorted_records(leaderboard)` is called without specifying the key (which is then assumed to be `leaderboard.options.sort_key`)

Example:
```lua
local leaderboard = leader_lib.create_leaderboard({p_name = "singleplayer", score = 0}, {
    uid_key = "p_name",
    ignore_threshold = 0,
    max_entries = -1,
    sort_key = "score",
    sort_direction = "ascending"
})
```

The if the example above is used, the returned leaderboard table is structured
like (after 3 records have been added):

```lua
local leaderboard = {
    records = {
        [1] = {p_name = "John" , score = 5, timestamp = 34534322},
        [2] = {p_name = "Billy", score = 3, timestamp = 34374327},
        [3] = {p_name = "Sue"  , score = 7, timestamp = 59439884}
    },
    options = {
        uid_key = "p_name",
        ignore_threshold = 0,
        max_entries = -1,
        sort_key = "score",
        sort_direction = "ascending"
    },
    record_template = {p_name = "singleplayer", score = 0, timestamp = 0},
}
```

### Storing leaderboards

Storage is not implemented in this library. Just serialize and deserialize the leaderboard table as needed.

### Updating the Leaderboard Automatically

```lua
leader_lib.add_or_update_record(leaderboard,attributes)
```

Automatically adds or updates a record, taking into account the `max_entries`, `uid_key`, `sort_key`, and `sort_direction`. If the record exists (if a record has the same `uid_key`), then updates the record only if the new `sort_key` is better than the old `sort_key` value. If the leaderboard is full, only adds the record if the record is better than the worst record. 

Example:
```lua
leader_lib.add_or_update_record(leaderboard, {p_name = "John", score = 100})
```

### Adding Records Manually


```lua
leader_lib.add_record(leaderboard, attributes)
```
Adds a record to the leaderboard with the given attributes and sets the timestamp.
If the leaderboard is full, only adds the record if the record is better than the worst record. 

- `leaderboard` (table): The leaderboard created using the `create_leaderboard` function.
- `attributes` (table): The attributes and values for the record.

Example:
```lua
leader_lib.add_record(leaderboard, {p_name = "John", score = 100})
```

### Updating Records Manually


```lua
leader_lib.update_record(leaderboard, idx, attributes)
```

Note: Updates the timestamp.

- `leaderboard` (table): The leaderboard created using the `create_leaderboard` function.
- `idx` (number): The index of the record to update.
- `attributes` (table): The attributes and values to update.

Example:
```lua
leader_lib.update_record(leaderboard, 1, {score = 150})
```

### Deleting Records


```lua
leader_lib.delete_record(leaderboard, idx)
```

- `leaderboard` (table): The leaderboard created using the `create_leaderboard` function.
- `idx` (number): The index of the record to delete.

Example:
```lua
leader_lib.delete_record(leaderboard, 1)
```

### Retrieving Records

To retrieve records from the leaderboard, you can use various functions:

#### `get_min_record_idx`

Returns the index of the record with the minimum value for the specified `key`.

```lua
leader_lib.get_min_record_idx(leaderboard, key)
```

- `leaderboard` (table): The leaderboard created using the `create_leaderboard` function.
- `key` (string, optional): The attribute key to find the minimum value. If not given, assumed to be `leaderboard.options.sort_key`

#### `get_max_record_idx`

Returns the index of the record with the maximum value for the specified `key`.

```lua
leader_lib.get_max_record_idx(leaderboard, key)
```

- `leaderboard` (table): The leaderboard created using the `create_leaderboard` function.
- `key` (string, optional): The attribute key to find the minimum value. If not given, assumed to be `leaderboard.options.sort_key`

#### `get_record_indices_by_value`

Returns an array of indices for records that have the specified `value` for the specified `key`.

```lua
leader_lib.get_record_indices_by_value(leaderboard, key, value)
```

- `leaderboard` (table): The leaderboard created using the `create_leaderboard` function.
- `key` (string): The attribute key to match the value against.
- `value` (any): The value to match.

#### `get_uid_index_by_value`

Returns the index of the record with the specified `value` in the `leaderboard.options.uid_key` key.

```lua
leader_lib.get_uid_index_by_value(leaderboard, value)
```

- `leaderboard` (table): The leaderboard created using the `create_leaderboard` function.
- `value` (any): The value to match in the UID key.

Examples:
```lua
-- returns the index of the record with the smallest score.
local min_idx = leader_lib.get_min_record_idx(leaderboard, "score")

-- returns the index of the record with the biggest score.
local max_idx = leader_lib.get_max_record_idx(leaderboard, "score")

-- returns a table of records with a score of 5
local scores = leader_lib.get_record_indices_by_value(leaderboard, "scores", 5) 

-- returns the index of the record with the player name "John", assuming the 
-- `leaderboard.options.uid_idx` stores playernames
local uid_index = leader_lib.get_uid_index_by_value(leaderboard, "John")
```

### Sorting Records

To retrieve sorted records from the leaderboard, use the `get_sorted_records` function. It takes three parameters: `leaderboard`, `key`, and `sorter`. The `key` parameter specifies the attribute key by which the records should be sorted. The `sorter` parameter is an optional function that determines the sorting order.

#### `get_sorted_records`
```lua
leader_lib.get_sorted_records(leaderboard, key, sorter)
```

- `leaderboard` (table): The leaderboard created using the `create_leaderboard` function.
- `key` (string, optional): The attribute key by which the records should be sorted. Defaults to the `sort_key` specified in the options.
- `sorter` (function, optional): The sorting function that determines the sorting order. Defaults to an ascending sort for the `sort_key`.

Returns an indexed table of records, such as that stored in the leaderboard itself, sorted as specified.
sorted records will contain an extra `idx` attribute with the original index of the leaderboard.

Examples:

Assuming the `leaderboard.options.sort_key` is `score`, returns the records sorted by score according to 
`leaderboard.options.sort_direction`:
```lua
local sorted_records = leader_lib.get_sorted_records(leaderboard)
```

Returns the records sorted by `p_name` in alphabetical order:
```lua
local sorted_records = leader_lib.get_sorted_records(leaderboard, "p_name")
```

Returns the records sorted by `p_name` in reverse alphabetical order:
```lua
local sorted_records = leader_lib.get_sorted_records(leaderboard, "p_name", function(a, b) return a < b end)
```

Delete the record with the 3rd highest score:
```lua
local sorted = leader_lib.get_sorted_records(leaderboard)
local idx_to_delete = sorted[3].idx
leader_lib.delete_record(leaderboard,idx_to_delete)
```

#### `get_top`

```lua
leader_lib.get_top(sorted_record_table,n)
```
- `sorted_record_table` (table): table returned by `get_sorted_records`
- `n` (integer): number of records to return

returns a subtable of the top n records

Example:
```lua
local top_5 = leader_lib.get_top(leader_lib.get_sorted_records(leaderboard),5) 
```

### Examples

This creates a leaderboard in which only one player can have one record, and is organized by score:

```lua
local leaderboard = leader_lib.create_leaderboard({p_name = "singleplayer", score = 0}, {
    uid_key = "p_name",
    ignore_threshold = 0,
    max_entries = -1,
    sort_key = "score",
    sort_direction = "ascending"
})

```

This creates a leaderboard that is organized by score, but each player can have more than one record:

```lua
local leaderboard = leader_lib.create_leaderboard({p_name = "singleplayer", score = 0}, {
    uid_key = "timestamp",
    ignore_threshold = 0,
    max_entries = -1,
    sort_key = "score",
    sort_direction = "ascending"
})
```

This creates a leaderboard for teams, organized by kills, which has nothing to do with p_name. 
Note that each team can only have one record:

```lua
local leaderboard = leader_lib.create_leaderboard({team_name = "red", kills = 0}, {
    uid_key = "team_name",
    ignore_threshold = 0,
    max_entries = -1,
    sort_key = "kills",
    sort_direction = "ascending"
})
```


