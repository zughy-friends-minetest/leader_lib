local function create_record( leaderboard , attributes )

    local record = table.copy(leaderboard.record_template)

    record.timestamp = os.time() -- builtin timestamp attribute

    for att_name, att_val in pairs(attributes) do
        if record[att_name] and type(att_val) == type(record[att_name]) then
            record[att_name] = att_val
        end
        -- TODO: add error message
    end

    return record
end

-- record_type (tbl): a template for the leaderboard record with default values.
-- Example: {p_name = "singleplayer", score = 0} 

-- options (optional) (tbl):

    -- uid_key (str): an attribute that should not be repeated on the
    -- leaderboard. Usually, you should set to a key that stores the player's
    -- name, assuming the leaderboard stores unique player scores. The default
    -- is "timestamp" since that is the only key provided by default, but you
    -- should change it so something more useful.
    
    -- ignore_threshold (number): default: 0 leaderboard.add_record is ignored
    -- if the value of the sort_key attribute is at or below (or above if
    -- sort_key is "max") this value. Useful for ignoring scores that are
    -- too low.
    
    -- max_entries (int): default: -1. -1 means no limit. Otherwise, the number
    -- or slots is limited to this amount.
    
    -- sort_key (str): default: "timestamp", The attribute key by which
    -- records are removed if there is a maximum record limit
    
    -- sort_direction (str): default "min", Either "min":smaller values are
    -- removed first, or "max":larger values are removed first, if there is a
    -- maximum record limit.


function leader_lib.create_leaderboard( record_template, options )

    local leaderboard = {}

    leaderboard.records = leaderboard.data.records or {}
    leaderboard.record_template = record_template
    leaderboard.record_template.timestamp = 0 -- builtin timestamp
    

    -- optional leaderboard params
    options = options or {}
    options.uid_key = "timestamp"
    options.ignore_threshold = 0
    options.max_entries = options.max_entries or -1 -- -1 means no limit. Other values specify the maximum number of entries in the leaderboard. Old entries will be kicked using the following params
    options.sort_key = options.sort_key or "timestamp" -- by default, removes old records by timestamp. E.g: "score"
    options.sort_direction = options.sort_direction or "ascending" -- or "descending". By default, removes old records by smallest timestamp. If the leaderboard is full, and the new record has a smaller timestamp than the minimum, then it will not be included. If "decending", then the largest value is considered "worst" and is removed first.
    
    leaderboard.options = options

    return leaderboard
end

function leader_lib.get_min_record_idx(leaderboard,key)
    key = key or leaderboard.options.sort_key
    local min_idx
    local min_val

    for rec_idx, rec in ipairs(leaderboard.records) do
        if not( min_val ) or (rec[key] < min_val) then
            min_val = rec[key]
            min_idx = rec_idx
        end
    end

    return min_idx
end


function leader_lib.get_max_record_idx(leaderboard,key)
    key = key or leaderboard.options.sort_key
    local max_idx
    local max_val

    for rec_idx, rec in ipairs(leaderboard.records) do
        if not( max_val ) or (rec[key] > max_val) then
            max_val = rec[key]
            max_idx = rec_idx
        end
    end

    return max_idx
end


function leader_lib.get_record_indices_by_value(leaderboard, key, value)

    local matching_records = {}

    for rec_idx, rec in ipairs(leaderboard.records) do
        if rec[key] == value then
            table.insert(matching_records, rec_idx)
        end
    end

    return matching_records

end


function leader_lib.get_uid_index_by_value(leaderboard, value)
    local idxs = leader_lib.get_record_indices_by_value(leaderboard,leaderboard.options.uid_key, value)
    return idxs[1] 
end


function leader_lib.update_record(leaderboard, idx, attributes)
    if leaderboard.records[idx] ~= nil then
        for att_name, att_val in pairs(attributes) do
            if leaderboard.records[idx][att_name] then
                leaderboard.records[idx][att_name] = att_val
                leaderboard.records[idx]["timestamp"] = os.time()
            end
        end
    end
end

-- deletes the record at idx
leader_lib.delete_record = function(leaderboard,idx)
    if leaderboard.records[idx] then
        table.remove(leaderboard.records,idx)
    end
end


function leader_lib.add_record( leaderboard , attributes )

    local rec = create_record( leaderboard , attributes )
    -- ignore new record if the sort_key is on the wrong side of the ignore_threshold

    if leaderboard.options.sort_direction == "ascending" then
        if rec[leaderboard.options.sort_key] <= leaderboard.options.ignore_threshold then
            return
        end
    elseif leaderboard.options.sort_direction == "descending" then
        if rec[leaderboard.options.sort_key] >= leaderboard.options.ignore_threshold then
            return
        end
    end

    if leaderboard.options.max_entries == -1 or 
        (leaderboard.options.max_entries > 0 and 
        (#leaderboard.records < leaderboard.options.max_entries)) then

        -- insert the record if there is no slot limit, or if the slot limit
        -- has not been reached

        table.insert( leaderboard.records , rec )

    else

        -- replace a record with the worst score if the slot limit has been
        -- reached
        if leaderboard.options.sort_direction == "ascending" then

            min_idx = leader_lib.get_min_record_idx(leaderboard,leaderboard.options.sort_key)
            min_val = leaderboard.records[min_idx][leaderboard.options.sort_key]
            
            if rec[leaderboard.options.sort_key] > min_val then
                leaderboard.records[min_idx] = rec
            end

        elseif leaderboard.options.sort_direction == "descending" then
            
            max_idx = leader_lib.get_max_record_idx(leaderboard,leaderboard.options.sort_key)
            max_val = leaderboard.records[max_idx][leaderboard.options.sort_key]
            
            if rec[leaderboard.options.sort_key] < max_val then
                leaderboard.records[max_idx] = rec
            end
        end
    end
end

function leader_lib.add_or_update_record(leaderboard, attributes)

    local existing_record_idx = leader_lib.get_uid_index_by_value(leaderboard, attributes[leaderboard.options.uid_key])
    if existing_record_idx then
        local existing_record = leaderboard.records[existing_record_idx]
        if leaderboard.options.sort_direction == "ascending" then
            if attributes[leaderboard.options.sort_key] >= existing_record[leaderboard.options.sort_key] then
                leader_lib.update_record(leaderboard, idx, attributes)
            end
        elseif leaderboard.options.sort_direction == "descending" then
            if attributes[leaderboard.options.sort_key] <= existing_record[leaderboard.options.sort_key] then
                leader_lib.update_record(leaderboard, idx, attributes)
            end
        end
    else
        leader_lib.add_record( leaderboard , attributes )
    end
    
end

function leader_lib.get_sorted_records( leaderboard, key, sorter)

    key = key or leaderboard.options.sort_key

    local default_sort = function(a,b)
        return a > b
    end

    if key == leaderboard.options.sort_key and leaderboard.options.sort_direction == "descending" then
        default_sort = function(a,b)
            return a < b
        end
    end

    sorter = sorter or default_sort

    local record_sorter = function(rec_a,rec_b)
        return sorter(rec_a[key],rec_b[key])
    end

    sorted_records = table.copy(leaderboard.records)

    for idx,rec in ipairs(sorted_records) do
        sorted_records[idx].idx = idx
    end

    table.sort(sorted_records,record_sorter)

    return sorted_records

end


function leader_lib.get_top(sorted_record_table,n)
    ret = {}
    for i=1,n do
        ret.insert(sorted_record_table)
    end
    return ret
end

